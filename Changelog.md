# Changelog

## 0.2.2
  - Dependency version bound updates
  - GHC 9.6 compatibility

## 0.2.1
  - Exchange some Mutable by Manifest restrictions for Massiv

## 0.2.0
  - Restructuring Modules

## 0.1.0
  - updates to Massiv 1.0.0.0; changes lots of array types.
  - infrastructure updates

## 0.0.2
  - switching from `PSQueue` to `psqueues` as the former is not maintained
  - raise upper bounds of base, allows GHC 9.0

## 0.0.1
  - raising upper bounds of optics and attoparsec

## 0.0.0
Initial version, providing PCA, DBScan and hierarchical clustering.
Command line interface to CREST XYZ trajectories.
