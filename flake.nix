{
  description = "ConClusion - Cluster analysis of conformere ensembles";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    nixBundlers.url = "github:NixOS/bundlers/master";
  };

  nixConfig.bash-prompt = ''\[\e[0;1;33m\]ConClusion\[\e[0;1m\]:\[\e[0;1;34m\]\w\[\e[0;1m\]$ \[\e[0m\]'';

  outputs = { self, nixpkgs, flake-utils, nixBundlers, ... }: flake-utils.lib.eachDefaultSystem
    (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ (import ./nix/overlay.nix) ];
        };

      in
      {

        packages = builtins.listToAttrs
          (builtins.map
            (ghcVersion: {
              name = "ConClusion${ghcVersion}";
              value = pkgs.haskell.packages."ghc${ghcVersion}".ConClusion;
            }) [ "810" "90" "92" "94" "96" "98" ]) // {
          default = pkgs.haskellPackages.ConClusion;
        };

        devShells.default = pkgs.haskellPackages.shellFor ({
          withHoogle = true;
          packages = p: [ p.ConClusion ];
          buildInputs = with pkgs; [
            cabal-install
            haskell-language-server
            haskellPackages.fourmolu
            haskellPackages.graphmod
            haskellPackages.calligraphy
            haskellPackages.ghc-prof-flamegraph
            haskellPackages.profiteur
            hlint
            hpack
            ghcid
            pkg-config
            graphviz
          ];
        });

        formatter = pkgs.nixpkgs-fmt;

        bundlers = {
          inherit (nixBundlers.bundlers."${system}") toArx toDEB toRPM toDockerImage;
          default = self.bundlers."${system}".toArx;
        };

      }) // {

    overlays.default = import ./nix/overlay.nix;

  };

}
