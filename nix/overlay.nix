final: prev:
{
  haskell = prev.haskell // {
    packageOverrides = hfinal: hprev: prev.haskell.packageOverrides hfinal hprev // {
      ConClusion = hfinal.callCabal2nix "ConClusion" ../. { };
    };
  };
}
