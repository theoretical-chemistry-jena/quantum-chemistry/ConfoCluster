{- |
Module      : ConClusion.Array.Conversion
Description : Type castings and conversions of array types
Copyright   : Phillip Seeber, 2023
License     : AGPL-3
Maintainer  : phillip.seeber@googlemail.com
Stability   : experimental
Portability : POSIX, Windows
-}
module ConClusion.Array.Conversion (
  vecH2M,
  vecM2H,
  matH2M,
  matM2H,
)
where

import Data.Massiv.Array as Massiv hiding (IndexException)
import Data.Massiv.Array.Manifest.Vector as Massiv
import Numeric.LinearAlgebra as LA hiding (magnitude, (<>))
import RIO
import qualified RIO.Vector.Storable as VectorS

-- | Converts a vector from the HMatrix package to the Massiv representation.
{-# SCC vecH2M #-}
vecH2M :: (Element e, Manifest r e, Load r Ix1 e) => VectorS.Vector e -> Massiv.Vector r e
vecH2M hVec = fromVector' Seq (Sz $ VectorS.length hVec) hVec

-- | Converts a vector from the Massiv representation to the HMatrix representation.
{-# SCC vecM2H #-}
vecM2H :: (Manifest r e, Load r Ix1 e, Element e) => Massiv.Vector r e -> LA.Vector e
vecM2H = Massiv.toVector

-- | Converts a matrix from the HMatrix representation to the Massiv representation.
{-# SCC matH2M #-}
matH2M :: (Manifest r e, Load r Ix1 e, Element e) => LA.Matrix e -> Massiv.Matrix r e
matH2M hMat = Massiv.resize' (Sz $ nRows :. nCols) . vecH2M . LA.flatten $ hMat
 where
  nRows = LA.rows hMat
  nCols = LA.cols hMat

-- | Converts a matrix from Massiv to HMatrix representation.
{-# SCC matM2H #-}
matM2H :: (Element e, Manifest r e, Load r Ix1 e) => Massiv.Matrix r e -> LA.Matrix e
matM2H mMat = LA.reshape nCols . vecM2H . Massiv.flatten $ mMat
 where
  Sz (_nRows :. nCols) = Massiv.size mMat
